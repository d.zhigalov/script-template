import * as ts from 'typescript';
import * as tstl from 'typescript-to-lua';

const oldCodeBlock = `local ____modules = {}
local ____moduleCache = {}
local ____originalRequire = require
local function require(file, ...)
    if ____moduleCache[file] then
        return ____moduleCache[file].value
    end
    if ____modules[file] then
        local module = ____modules[file]
        ____moduleCache[file] = { value = (select("#", ...) > 0) and module(...) or module(file) }
        return ____moduleCache[file].value
    else
        if ____originalRequire then
            return ____originalRequire(file)
        else
            error("module '" .. file .. "' not found")
        end
    end
end`;

const newCodeBlock = `local ____modules = {}
local ____moduleCache = {}
local function require(file, ...)
    if ____moduleCache[file] then
        return ____moduleCache[file].value
    end
    if ____modules[file] then
        local module = ____modules[file]
        ____moduleCache[file] = { value = module(file) }
        return ____moduleCache[file].value
    end
end`;

let noRequireMainCode = '';
const returnCode = /return require\("main", ...\)\n?/;
const mainModuleCode = `["main"] = function(...) 

 end,
`;

const requireRegex = /require\([^\)]*\)/g;

const plugin: tstl.Plugin = {
  afterPrint(program, options, emitHost, result) {
    for(const file of result) {
      for(const sourceFile of file.sourceFiles ? file.sourceFiles : []) {
        if(sourceFile.fileName.endsWith('main.ts')) {
          noRequireMainCode = file.code.replace(requireRegex, 'require()');
        }
      }
    }
  },
  beforeEmit(program, options, emitHost, result) {
    for(const file of result) {
      // Use modified require function
      file.code = file.code.replace(oldCodeBlock, newCodeBlock);
      //Match main code based on no require version
      const noRequireCode = file.code.replace(requireRegex, 'require()');
      let startIndex = noRequireCode.indexOf(noRequireMainCode);
      let endIndex = startIndex + noRequireMainCode.length;
      let startLine = noRequireCode.substring(0, startIndex).split('\n').length;
      let endLine = noRequireCode.substring(0, endIndex).split('\n').length;
      //Find real main code
      let realMainCode = file.code.split('\n').filter((s, i) => i >= startLine - 1 && i < endLine - 1).reduce((p,c) => p + '\n' + c);
      //Remove main code from modules table
      file.code = file.code.replace(realMainCode, '');
      file.code = file.code.replace(mainModuleCode, '');
      //Globalize Update inside main if needed
      realMainCode = realMainCode.replace('local function Update(I)', 'function Update(I)');
      //Remove exports inside main if needed
      realMainCode = realMainCode.replace(/local ____exports = {}\n?/, '');
      realMainCode = realMainCode.replace(/return ____exports\n?/, '');
      //Replace return code with modified main code
      file.code = file.code.replace(returnCode, realMainCode);
      //Rename all require calls to require2
      file.code = file.code.replace(/require\(/g, 'require2(');
    }
  }
}

export default plugin;